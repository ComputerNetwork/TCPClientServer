idfile.txt - File containing NEU IDs . Format: 10 digit NEU ID per line
secrefile.txt - File containig Secret Flags. Format: 64byte flag per line.
Assumption is the line number of NEU ID in idfile and corresponing Secret in secret file
are same.
Max lines are assumed to be 50, we can modify it in the program.

Setup:
======
Place idfile.txt and secretfile.txt in the same folder as server.c

To launch client to get get secret from ccs machine,
  make claunch

To launch server,
  make slaunch

To launch client that accesses the server
  make lclaunch

secret.txt contains the secret flag retrieved by my client program from ccs
machine.
