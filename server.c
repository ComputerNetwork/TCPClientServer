/*
** server.c -- a stream socket server demo
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <errno.h>
#include <time.h>
#include <stdbool.h>

#define HOST_STR_LEN 100
#define ID_LEN 11
#define PORT_LEN 6
#define BACKLOG 10
#define BUFFSIZE 512
#define DEFAULT_PORT "27993"
#define NUM_OPERATIONS 100
#define OPERATOR_MAX_VALUE 1000
#define NUM_OPERATORS 4
#define MAX_LINES 50
#define MAX_LINE_LEN 100

#define DBG 1

char NEU_ID[ID_LEN];
char IDFILE[HOST_STR_LEN];
char SECRETFILE[HOST_STR_LEN];
char PORT[PORT_LEN];
char HOSTNAME[HOST_STR_LEN];

//Handle Finished processes
void sigchld_handler(int s)
{
    int stat;
	while(waitpid(-1, &stat, WNOHANG) > 0){
        if(stat != 0){
          printf("Client Failed with error: %d\n", stat);
        }
		continue;	
	}
}

/*
 * Read the file and load it to char array
 * Args:
 *  list - 2D array to hold ID/FLAG
 *  filename - Name of the file
 * Returns:
 *  None
 * */
void load_list(char list[MAX_LINES][MAX_LINE_LEN], char *filename){
	FILE *fp = fopen(filename, "r"); 
	char * line = NULL;
	size_t len = 0;
	ssize_t read;

	if (fp == NULL)
	exit(EXIT_SUCCESS);
	int i = 0;
	while ((read = getline(&line, &len, fp)) != -1) {    
		line[strlen(line) - 1] = '\0';
		strcpy(list[i++], line);
	}

	fclose(fp);
	if (line)
		free(line);
}

/*
 * Receive hello message from Client
 * Args:
 *  fd - Socket FD
 * Returns:
 *  None
 * */
void recv_hello_message(int fd){
	char recv_buff[BUFFSIZE];
	int numbytes;
	if ((numbytes = recv(fd, recv_buff, BUFFSIZE-1, 0)) == -1) {
		perror("recv");
		exit(EXIT_FAILURE);
	}
	recv_buff[numbytes] = '\0';
	if(sscanf(recv_buff, "cs5700fall2017 HELLO %s\n", NEU_ID) <= 0)	{
		printf("Error: %d;Unknown Message %s\n", errno, recv_buff);		
		exit(EXIT_SUCCESS);
	}
}

/*
 * Receive SOLUTION from Client
 * Args:
 *  fd - Socket FD
 * Returns:
 *  None
 * */
int recv_solution_message(int fd){
	int ans;
	char recv_buff[BUFFSIZE];
	int numbytes;
	if ((numbytes = recv(fd, recv_buff, BUFFSIZE-1, 0)) == -1) {
		perror("recv");
		exit(EXIT_FAILURE);
	}
	recv_buff[numbytes] = '\0';
	if(sscanf(recv_buff, "cs5700fall2017 %d\n", &ans) <= 0)	{
		printf("Error: %d;Unknown Message %s\n", errno, recv_buff);		
		exit(EXIT_SUCCESS);
	}
	return ans;
}
/*
 * Find the answer from A (OP) B
 * Args:
 *  opA - Operand A
 *  opB - Operand B
 *  operator - Math operator.
 * Returns:
 *  Computes the operation and returns closes integer
 * */
int find_answer(int opA, int opB, char operator){
	int ans = 0;
	switch(operator){
		case '+' : 
			ans = opA + opB;
			break;
		case '-' :
			ans = opA - opB;
			break;
		case '*' :
			ans = opA * opB;
			break;
		case '/' :
			ans = opA / opB;		
			break;
		default:
			printf("Invalid operator\n");
			break;
	}
	printf("Answer is %d\n", ans);
	return ans;
}

// get sockaddr, IPv4:
void *get_in_addr(struct sockaddr *sa)
{
	return &(((struct sockaddr_in*)sa)->sin_addr);
}

/*
 * Given NEUID, find Corresponding Line number in the file
 * */
int find_id_index(char list[MAX_LINES][MAX_LINE_LEN], int n){
	int i = 0;
	for(; i < n; i++){
		if(strcmp(list[i], NEU_ID) == 0)
			return i;	
	}
	return -1;
}

/*
 * Prints program usage
 * */ 
void usage(){
	printf("Usage: ./server <-p port> [hostname] [file NEUID] [file FLAG]\n");
	exit(EXIT_FAILURE);
}

/*
 * Parse the command line arguments
 * Args:
 *  argc - Arguments count
 *  argv - Array of char arrays
 * Returns:
 *  None
 *  Just sets the static variables accordingly.
 * */
void parse_args(int argc, char **argv){
    int c;
	bool pFlag = false;
	if(argc < 4){
		printf("Not enough args");	
		usage();
	}
	else if(argc == 6){
		strcpy(HOSTNAME, argv[3]);
		strcpy(IDFILE, argv[4]);
		strcpy(SECRETFILE, argv[5]);
		strcpy(PORT, argv[2]);
	}
	else{
		strcpy(HOSTNAME, argv[1]);
		strcpy(IDFILE, argv[2]);
		strcpy(SECRETFILE, argv[3]);
		strcpy(PORT, DEFAULT_PORT);	
	}
}

/*
 * Open Socket Connection
 * */
int open_connection(){

	int sockfd;
	struct addrinfo hints, *servinfo, *p;

	int rv;
	int yes = 1;
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE; 

	if ((rv = getaddrinfo(NULL, PORT, &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		return -1;
	}

	for(p = servinfo; p != NULL; p = p->ai_next) {
        //Create Socket FD
        if ((sockfd = socket(p->ai_family, p->ai_socktype,
				p->ai_protocol)) == -1) {
			perror("server: socket");
			continue;
		}

		if (setsockopt(sockfd, SOL_SOCKET, \
						SO_REUSEADDR, &yes, sizeof(int)) == -1) {
			perror("setsockopt");
			exit(EXIT_FAILURE);
		}
        //Bind to the address we got
		if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			close(sockfd);
			perror("server: bind");
			continue;
		}

		break;
	}

    //clear the struct
	freeaddrinfo(servinfo);

	if (p == NULL)  {
		fprintf(stderr, "server: failed to bind\n");
		exit(EXIT_FAILURE);
	}

    //start listening on the FD.
	if (listen(sockfd, BACKLOG) == -1) {
		perror("listen");
		exit(EXIT_FAILURE);
	}
	return sockfd;
}

/*
 * Send STATUS message, Check the result and find if the client is valid. 
 * */
int is_client_valid(int new_fd){

	int is_ans_invalid = 0;
	char operators[NUM_OPERATORS+1] = "+-*/";
	//Recv Hello Message and get NEU ID.
	recv_hello_message(new_fd);
	//Pick a random number(rand) from 100-200
	int count = NUM_OPERATIONS + (rand() % NUM_OPERATIONS);
	int i;
	//For i from 1 to rand
	for(i = 0; i < count; i++){
		//pick 2 random numbers from 1 to 1000. 
		int opA = 1 + (rand() % OPERATOR_MAX_VALUE);
		int opB = 1 + (rand() % OPERATOR_MAX_VALUE);
		//and an operator at random (+, -, *, /)
		char op = operators[rand()%NUM_OPERATORS];
		char send_buff[BUFFSIZE];
		int ans = find_answer(opA, opB, op);
		snprintf(send_buff, BUFFSIZE, \
					"cs5700fall2017 STATUS %d %c %d\n", opA, op, opB);
		printf("Sending : %s\n", send_buff);
		//compose status message and send
		if (send(new_fd, send_buff, strlen(send_buff), 0) == -1){
			perror("send");
			exit(EXIT_FAILURE);
		}
		//Recv solution message				
		//continue if matches. else break.
		if(recv_solution_message(new_fd) == ans)
			continue;
		else{
			printf("Invalid answer... Terminating Connection\n");
			return 0;
		}
	}
	return 1;
}

/*
 * Handle client connection.
 * */
void handle_client(int sockfd, int new_fd, \
					char IDList[MAX_LINES][MAX_LINE_LEN],\
					char SecretList[MAX_LINES][MAX_LINE_LEN]){
	srand(time(NULL));
	close(sockfd); 
	//After loop, send the secret flag.
	if(is_client_valid(new_fd)){
		int index = find_id_index(IDList, MAX_LINES);
		char secret[BUFFSIZE];	
		if(index < 0){
			printf("Couldnt find Index %s\n", NEU_ID);
			snprintf(secret, BUFFSIZE, "cs5700fall2017 Unknown_Husky_ID BYE\n");				
		}
		else{
			printf("SecretFlag: %s\n", SecretList[index]);					
			snprintf(secret, BUFFSIZE, \
						"cs5700fall2017 %s BYE\n", SecretList[index]);
		}
		if (send(new_fd, secret, strlen(secret), 0) == -1)
			perror("send");
			exit(EXIT_SUCCESS);
	}
    else{
        printf("Invalid Answer Exiting...");
        exit(EXIT_FAILURE);
    }
	close(new_fd);
	exit(EXIT_SUCCESS);
}

int main(int argc, char **argv)
{
	parse_args(argc, argv);
	int sockfd, new_fd;  
	struct sockaddr_storage their_addr; 
	socklen_t sin_size;
	char recv_buff[BUFFSIZE], send_buff[BUFFSIZE];
	char IDList[MAX_LINES][MAX_LINE_LEN], SecretList[MAX_LINES][MAX_LINE_LEN];
	load_list(IDList, IDFILE);
	load_list(SecretList, SECRETFILE);
	signal(SIGCHLD,sigchld_handler);
	sockfd = open_connection();
	printf("server: waiting for connections...\n");
	while(1) {  
		sin_size = sizeof(their_addr);
		new_fd = accept(sockfd, (struct sockaddr *)&their_addr, &sin_size);
		if (new_fd == -1) {
			perror("accept");
			continue;
		}

		#ifdef DBG
		char client_addr[INET_ADDRSTRLEN];
		inet_ntop(their_addr.ss_family, \
				get_in_addr((struct sockaddr *)&their_addr), \
				client_addr, sizeof client_addr);
		printf("server: got connection from %s\n", client_addr);
		#endif

		pid_t pid = fork();	
		if(pid < 0){
			perror("fork");
			exit(-1);		
		}	
		if (pid == 0) {
			//child Process 
			handle_client(sockfd, new_fd, IDList, SecretList);
		}
		close(new_fd); 
	}

	return 0;
}

