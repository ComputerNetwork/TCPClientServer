
client: client.c
	gcc -g client.c -o client

claunch: client
	./client -p 27993 cs5700f17.ccs.neu.edu 001648794

lclaunch: client
	./client -p 27993 localhost 001647895

lclaunch1: client
	./client -p 27993 localhost 001647894

server: server.c
	gcc -g server.c -o server

slaunch: server
	./server -p 27993 localhost ./idfile.txt ./secretfile.txt

clean: 
	rm -rf ./server ./client ./lclient ./core

default: claunch 
