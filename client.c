/*
* TCP client to get status, send result and retireve secret flag
* */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <getopt.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <stdbool.h>
#include <arpa/inet.h>

//Enable this for debugging
//#define DBG 1

#define HOST_STR_LEN 100
#define ID_LEN 11
#define PORT_LEN 6

#define DEFAULT_PORT "27993"
#define BUFFSIZE 512 
#define STATUS_HEADER "cs5700fall2017 STATUS"
#define BYE_HEADER "cs5700fall2017 "
#define HELLO_MESSAGE "cs5700fall2017 HELLO"
#define SECRET_FLAG_SIZE 64

char PORT[PORT_LEN];
char HOSTNAME[HOST_STR_LEN];
char NEUID[ID_LEN];

// get sockaddr, IPv4 
void *get_in_addr(struct sockaddr *sa)
{
	return &(((struct sockaddr_in*)sa)->sin_addr);    
}

/*
 * Computes answer from Status Message
 * Args:
 *  status_msg - String in following format "opA operator opB\n"
 * Returns:
 *  Performs math operation and returns closest integer
 * */
int compute_answer(char *status_msg){
	int opA, opB;
	char operator;
	sscanf(status_msg, "%d %c %d\n", &opA, &operator, &opB);
	int ans = 0;
	switch(operator){
		case '+' : 
			ans = opA + opB;
			break;
		case '-' :
			ans = opA - opB;
			break;
		case '*' :
			ans = opA * opB;
			break;
		case '/' :
			ans = opA / opB;		
			break;
		default:
			printf("Invalid operator\n");
			exit(EXIT_FAILURE);
			break;
	}
	#ifdef DBG
	printf("Answer is %d\n", ans);
	#endif
	return ans;
}

/*
 * Send Hello Message to Server
 * Args:
 *  sockfd - Socket FD
 *  send_buff - String buffer to be sent
 * Returns:
 *  None
 * HELLO MESSAGE :> cs5700fall2017 HELLO 001648794\n
 * */
void send_hello(int sockfd, char *send_buff){
	int numbytes;
	snprintf(send_buff, BUFFSIZE-1, "%s %s\n", HELLO_MESSAGE, NEUID);

	if((numbytes = send(sockfd, send_buff, strlen(send_buff), 0)) == -1) {
		perror("send");
		exit(EXIT_FAILURE);
	}
}

/*
 * Receive message from Server
 * Args:
 *  sockfd    - Socket FD
 *  recv_buff - Buffer to store message from server
 *  bye_header_len  - Length of BYE header (cs5700fall2017 )
 * Returns:
 *  None
 * */
void recv_from_server(int sockfd, char *recv_buff, int bye_header_len){
	int numbytes;
	if ((numbytes = recv(sockfd, recv_buff, BUFFSIZE-1, 0)) == -1) {
		perror("recv");
		exit(EXIT_FAILURE);
	}
	recv_buff[numbytes] = '\0';
	if(numbytes < bye_header_len){
		printf("We havent received a status/bye message...Exiting\n");
		printf("RECEIVED: %s\n", recv_buff);
		exit(EXIT_FAILURE);		
	}
	
}

/*
 * Process BYE message and get secret flag
 * Args:
 *  recv_buff - Buffer to with message from server
 *  bye_header_len  - Length of BYE header (cs5700fall2017 )
 * Returns:
 *  Prints Secret flag
 * */
void process_bye_message(char *recv_buff, int bye_header_len){
	char secret_flag[SECRET_FLAG_SIZE+1];
	strncpy(secret_flag, &recv_buff[bye_header_len], SECRET_FLAG_SIZE);
	secret_flag[SECRET_FLAG_SIZE] = '\0';
	printf("%s\n", secret_flag);
}

/*
 * Compute the solution from status message and return the answer 
 * Args:
 *  sockfd  - Socket FD
 *  recv_buff - Buffer to store message from server
 *  send_buff - Buffer to hold message to be sent to server
 *  status_header_len  - Length of STATUS header (cs5700fall2017 STATUS)
 * Returns:
 *  None
 * */
void find_and_send_solution(int sockfd, char *recv_buff, char *send_buff,\
							 int status_header_len){
	int answer = compute_answer(&recv_buff[status_header_len]);
	int numbytes;
	char ans_str[BUFFSIZE];
	memset(send_buff, 0, BUFFSIZE-1);
	snprintf(send_buff, BUFFSIZE-1, "%s %d\n", BYE_HEADER, answer);
	send_buff[strlen(send_buff)] = '\0';	
	#ifdef DBG
	printf("Solution message:%s", send_buff);		
	#endif
	if((numbytes = send(sockfd, send_buff, strlen(send_buff), 0)) == -1) {
		perror("send");
		exit(EXIT_FAILURE);
	}
}

/*
 * Prints program usage
 * */ 
void usage(){
	printf("Usage: ./client <-p port> [hostname] [NEUID]\n");
	exit(EXIT_FAILURE);
}

/*
 * Parse the command line arguments
 * Args:
 *  argc - Arguments count
 *  argv - Array of char arrays
 * Returns:
 *  None
 *  Just sets the static variables accordingly.
 * */
void parse_args(int argc, char **argv){
    int c;
	bool pFlag = false;
	if(argc < 3){
		printf("Not enough args");	
		usage();
	}
	else if(argc == 5){
		strcpy(HOSTNAME, argv[3]);
		strcpy(NEUID, argv[4]);
		strcpy(PORT, argv[2]);
	}
	else{
		strcpy(HOSTNAME, argv[1]);
		strcpy(NEUID, argv[2]);		
		strcpy(PORT, DEFAULT_PORT);	
	}
}

/*
 * Opens a new connection to server
 * Returns Socket FD
 * */
int open_connection(){
	int sockfd;
	struct addrinfo hints, *servinfo, *p;
	int rv;
	char server_addr[INET_ADDRSTRLEN];
	//Create a TCP socket (IPv4) 
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;

    //populate the server info in servinfo struct
	if((rv = getaddrinfo(HOSTNAME, PORT, &hints, &servinfo)) != 0) {
		printf("Error getting addr info: %s\n", gai_strerror(rv));
		return -1;
	}

    //Iterate through the list and open valid connection
	for(p = servinfo; p != NULL; p = p->ai_next) {
		if((sockfd = socket(p->ai_family, p->ai_socktype,p->ai_protocol)) == -1)
		{
			perror("client: socket");
			continue;
		}

		if(connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			close(sockfd);
			perror("client: connect");
			continue;
		}

		break;
	}
    //We haven't been able to open connection.
	if(p == NULL) {
		return -1;
	}

	#ifdef DBG
	inet_ntop(p->ai_family, get_in_addr((struct sockaddr *)p->ai_addr),
	server_addr, sizeof(server_addr));
	printf("client: connecting to %s\n", server_addr);
	#endif
    
    //Clear servinfo struct
	freeaddrinfo(servinfo);
	return sockfd;
}

int main(int argc, char *argv[])
{
	parse_args(argc, argv);

	int sockfd;  
	char recv_buff[BUFFSIZE], send_buff[BUFFSIZE];

	int status_header_len = strlen(STATUS_HEADER);
	int bye_header_len = strlen(BYE_HEADER);

	sockfd = open_connection();
	if(sockfd == -1){
		printf("Failed to open a socket\n");
		exit(EXIT_FAILURE);	
	}
	sleep(1);
    //Send a Hello message
	send_hello(sockfd, send_buff);

	while(1){
        //Receive message from server
		recv_from_server(sockfd, recv_buff, bye_header_len);
		#ifdef DBG
		printf("client: received :%s",recv_buff);
		#endif
        //Check if its a BYE message (cs5700fall2017 <flag> BYE )
		if(strstr(recv_buff, "BYE") != NULL) {
			process_bye_message(recv_buff, bye_header_len);
			break;
		}
		else{
            //STATUS message. So find solution and return.
			find_and_send_solution(sockfd, \
									recv_buff, send_buff, status_header_len);
		}
		memset(recv_buff, 0, BUFFSIZE-1);
	}	
	close(sockfd);

	return 0;
}
